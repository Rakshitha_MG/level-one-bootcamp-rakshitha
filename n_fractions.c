/#include<stdio.h>
struct fract
{
int x;
int y;
};
typedef struct fract fraction;
fraction input()
{
fraction a;
printf("Enter the numerator\n");
scanf("%d",&a.x);
printf("Enter the denominator\n");
scanf("%d",&a.y);
return a;
}
fraction sum(fraction a,fraction b)
{
fraction res;
if(a.y==b.y)
{
res.y=a.y;
res.x=a.x+b.x;
}
else
{
res.y=a.y*b.y;
res.x=(a.x*b.y)+(b.x*a.y);
}
return res;
}
int main()
{
int n;
printf("Enter the number of fractions\n");
scanf("%d",&n);
fraction c,a[n];
c.x=0;
c.y=1;
for(int i=0;i<n;i++)
{
    printf("for fraction %d\n",(i+1));
    a[i]=input();
}
for(int i=0;i<n;i++)
{
    c=sum(c,a[i]);
}
printf("The total sum is %d%d which is %.2f",c.x,c.y,(c.x/(1.0*c.y))*100);
return 0;
}
/WAP to find the sum of n fractions.