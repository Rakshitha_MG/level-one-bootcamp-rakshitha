#include<stdio.h>
#include<conio.h>

typedef struct fraction
{
   int n;
   int d;
}fraction;

fraction input()
{
  fraction a;
  printf("Enter the numerator: ");
  scanf("%d",&a.n);
  printf("Enter the denominator: ");
  scanf("%d",&a.d);
  return a;
}

int gcd(int x,int y)
{
  int i,gcd=1;
  for(i=2; i<=x && i<=y; ++i)
  {
    if(x%i==0 && y%i==0)
    gcd=i;
  }
  return gcd;
}

fraction sum( fraction f1, fraction f2 )
{ 
  int c;
  fraction f3;
  f3.n=((f1.n*f2.d)+(f2.n*f1.d));
  f3.d=(f1.d*f2.d);
  c=gcd(f3.n,f3.d);
  f3.n=f3.n/c;
  f3.d=f3.d/c;
  return f3;
}
void output( fraction f1, fraction f2, fraction f3)
{
  printf(" %d/%d + %d/%d is %d/%d", f1.n,f1.d,f2.n,f2.d,f3.n,f3.d);
}
int main()
{
  fraction fr1,fr2,fr3;
  printf("Enter the first fraction :\n");
  fr1=input();
  printf("Enter the second fraction: \n");
  fr2=input();
  fr3=sum(fr1,fr2);
  output(fr1,fr2,fr3);
  return 0;
}
//WAP to find the sum of two fractions.